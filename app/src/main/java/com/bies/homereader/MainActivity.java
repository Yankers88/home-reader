package com.bies.homereader;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.widget.CompoundButton.*;
import static com.bies.homereader.R.layout.devicecell;

@SuppressWarnings("ALL")
public class MainActivity extends Activity {

    TextView serialNumber,mac,softVersion,temperature,humidity,light,fireAlarmTemperature,freezeAlarmTemperature;
    String text,sn="",mc="",sv="",temp="",hum="",l="",fireAT="",freezeAT="";
    Section[] sections;
    Room[] rooms;
    Device[] devices;
    Device[] devices2;
    String user="admin";
    String password = "admin";
    ListView listView;
    Context context;
    int changesst;
    long startTime = 0;
    int last;
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            if(changesst==0)
                new ChangesGet().execute();
            timerHandler.postDelayed(this, 500);
        }
    };
    int getsectionid(int id,Section[] section)
    {
        for(int i=0;i<section.length;i++)
        {
            if(id == section[i].id)
                return i;
        }
        return -1;
    }
    int getroomid(int id,Room[] section)
    {
        for(int i=0;i<section.length;i++)
        {
            if(id == section[i].id)
                return i;
        }
        return -1;
    }
    int getdeviceid(int id,Device[] section)
    {
        for(int i=0;i<section.length;i++)
        {
            if(id == section[i].id)
                return i;
        }
        return -1;
    }
    int[] arrayintadd(int element , int[] oldarray)
    {
        int[] out;
        if(oldarray==null)
        {
            out = new int[1];
            out[0] = element;
        }
        else
        {
            out = new int[oldarray.length+1];
            for(int i=0;i<oldarray.length;i++)
            {
                out[i] = oldarray[i];
            }
            out[oldarray.length] = element;
        }
        return out;


    }
    @Override
    public void onPause() {
        super.onPause();
        timerHandler.removeCallbacks(timerRunnable);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changesst=0;
        last=0;
        serialNumber = (TextView) findViewById(R.id.serial_number);
        mac = (TextView) findViewById(R.id.mac);
        softVersion = (TextView) findViewById(R.id.softVersion);
        temperature = (TextView) findViewById(R.id.temperature);
        humidity = (TextView) findViewById(R.id.humidity);
        light = (TextView) findViewById(R.id.light);
        fireAlarmTemperature = (TextView) findViewById(R.id.fireAlarmTemperature);
        freezeAlarmTemperature = (TextView) findViewById(R.id.freezeAlarmTemperature);
        listView = (ListView) findViewById(R.id.list);
        context = this;
        new HomeConnector().execute();
    }

    String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException
    {
        InputStream in = entity.getContent();
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }
    void connect(String url){
        try
        {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            AuthScope authScope=new AuthScope("217.168.133.115",8080);
            UsernamePasswordCredentials credentials=new UsernamePasswordCredentials(user,password);
            httpclient.getCredentialsProvider().setCredentials(authScope,credentials);
            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute("http.user-token",user+":"+password);
            HttpGet httpGet = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpGet, localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        }
        catch (Exception e)
        {
        }
    }
    String sconnect(String url){
        String out=null;
        try
        {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            AuthScope authScope=new AuthScope("217.168.133.115",8080);
            UsernamePasswordCredentials credentials=new UsernamePasswordCredentials(user,password);
            httpclient.getCredentialsProvider().setCredentials(authScope,credentials);
            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute("http.user-token",user+":"+password);
            HttpGet httpGet = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpGet, localContext);
            HttpEntity entity = response.getEntity();
            out = getASCIIContentFromEntity(entity);
        }
        catch (Exception e)
        {
        }
        return out;
    }
    private class HomeConnector extends AsyncTask<Void, Void, String>
    {

        @Override
        protected String doInBackground(Void... params) {
            //get info
            String surl = "http://217.168.133.115:8080/api/settings/info";
            text = null;
            connect(surl);
            try {
                JSONObject jsonObj;
                jsonObj = new JSONObject(text);
                sn="Numer seryjny: "+jsonObj.getString("serialNumber");
                mc="Adres mac: " + jsonObj.getString("mac");
                sv="Wersja oprogramowania: " + jsonObj.getString("softVersion");

            } catch (Exception e)
            {
            }
            //get home info
            surl = "http://217.168.133.115:8080/api/home";
            text = null;
            connect(surl);
            try {
                JSONObject jsonObj;
                jsonObj = new JSONObject(text);
                JSONObject ds = jsonObj.getJSONObject("defaultSensors");
                temp="Temperatura: "+ds.getString("temperature");
                hum="Wilgotność: "+ds.getString("humidity");
                l="Światła: "+ds.getString("light");
                fireAT="Temperatura alarmu przeciwpożarowego: "+jsonObj.getString("fireAlarmTemperature");
                freezeAT="Temperatura zamarzania: "+jsonObj.getString("freezeAlarmTemperature");
            } catch (Exception e)
            {
            }
            //get sections
            surl = "http://217.168.133.115:8080/api/sections";
            text = null;
            connect(surl);
            try {
                JSONArray sections_json = new JSONArray(text);
                sections = new Section[sections_json.length()];
                for (int i = 0; i < sections_json.length(); i++) {
                    sections[i] = new Section();
                    JSONObject c = sections_json.getJSONObject(i);
                    sections[i].id = Integer.parseInt(c.getString("id"));
                    sections[i].name = c.getString("name");
                    sections[i].sortOrder = Integer.parseInt(c.getString("sortOrder"));
                }
            } catch (Exception e)
            {
            }
            //get rooms
            surl = "http://217.168.133.115:8080/api/rooms";
            text = null;
            connect(surl);
            try {
                JSONArray sections_json = new JSONArray(text);
                rooms = new Room[sections_json.length()];
                for (int i = 0; i < sections_json.length(); i++) {
                    rooms[i] = new Room();
                    JSONObject c = sections_json.getJSONObject(i);
                    rooms[i].id = Integer.parseInt(c.getString("id"));
                    rooms[i].name = c.getString("name");
                    rooms[i].icon = c.getString("icon");
                    rooms[i].defaultThermostat = c.getString("defaultThermostat");
                    rooms[i].sectionID = Integer.parseInt(c.getString("sectionID"));
                    int sid = getsectionid(rooms[i].sectionID ,sections);
                    if(sid!=-1)
                        sections[sid].room_ids = arrayintadd(i,sections[sid].room_ids);
                    rooms[i].sortOrder = Integer.parseInt(c.getString("sortOrder"));
                    rooms[i].defaultSensors = new Sensor[3];
                    JSONObject defsensors = new JSONObject(c.getString("defaultSensors"));
                    rooms[i].defaultSensors[0] = new Sensor();
                    rooms[i].defaultSensors[0].name = "temperature";
                    rooms[i].defaultSensors[0].value = Float.parseFloat(defsensors.getString("temperature"));
                    rooms[i].defaultSensors[1] = new Sensor();
                    rooms[i].defaultSensors[1].name = "humidity";
                    rooms[i].defaultSensors[1].value = Float.parseFloat(defsensors.getString("humidity"));
                    rooms[i].defaultSensors[2] = new Sensor();
                    rooms[i].defaultSensors[2].name = "light";
                    rooms[i].defaultSensors[2].value = Float.parseFloat(defsensors.getString("light"));
                }
            } catch (Exception e)
            {
            }

            //get devices
            surl = "http://217.168.133.115:8080/api/devices";
            text = null;
            connect(surl);
            try {
                JSONArray sections_json = new JSONArray(text);
                devices = new Device[sections_json.length()];
                for (int i = 0; i < sections_json.length(); i++) {
                    devices[i] = new Device();
                    JSONObject c = sections_json.getJSONObject(i);
                    devices[i].id = Integer.parseInt(c.getString("id"));
                    devices[i].roomID = Integer.parseInt(c.getString("roomID"));
                    int sid = getroomid(devices[i].roomID, rooms);
                    if(sid!=-1)
                        rooms[sid].device_ids = arrayintadd(i,rooms[sid].device_ids);
                    devices[i].name = c.getString("name");
                    devices[i].type = c.getString("type");
                    JSONObject properties = new JSONObject(c.getString("properties"));
                    if(properties.has("value"))
                        devices[i].value = properties.getString("value");
                }
            } catch (Exception e)
            {
            }

            return null;
        }
        @Override
        protected void onPostExecute(String result) {

            serialNumber.setText(sn);
            mac.setText(mc);
            softVersion.setText(sv);
            temperature.setText(temp);
            humidity.setText(hum);
            light.setText(l);
            fireAlarmTemperature.setText(fireAT);
            freezeAlarmTemperature.setText(freezeAT);
            listView = (ListView) findViewById(R.id.list);
            devices2 = get_lights(devices);
            DeviceAdapter adapter = new DeviceAdapter(context,devicecell, names(devices2));
            listView.setAdapter(adapter);

            startTime = System.currentTimeMillis();
            timerHandler.postDelayed(timerRunnable, 0);

        }
    }
    private class ChangesGet extends AsyncTask<Void, Void, String>
    {
        JSONArray changes;
        int[] changed_id;
        boolean haschanges=false;
        String surl = "http://217.168.133.115:8080/api/refreshStates?last="+last;
        @Override
        protected String doInBackground(Void... params) {
            changesst=1;
            text = null;
            connect(surl);
            try {
                JSONObject c = new JSONObject(text);
                last = Integer.parseInt(c.getString("last"));
                if(c.has("changes")) {
                    haschanges=true;
                    changes = c.getJSONArray("changes");
                    changed_id = new int[changes.length()];
                    for (int i = 0; i < changes.length(); i++) {
                        changed_id[i] = Integer.parseInt(changes.getJSONObject(i).getString("id"));
                    }
                }


            } catch (Exception e)
            {

            }

            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(haschanges) {
                for (int i = 0; i < devices2.length; i++) {
                    for (int j = 0; j < changed_id.length; j++) {
                        if (devices2[i].id == changed_id[j]) {
                            devices2[i].update();
                        }
                    }
                }
            }
            changesst=0;
        }
    }
    Device[] get_lights(Device[] devices)
    {
        int count=0;
        for (int i=0;i<devices.length;i++) {
            if(devices[i].type.equals("dimmable_light") || devices[i].type.equals("binary_light"))
                count++;
        }
        Device[] out = new Device[count];
        count=0;
        for (int i=0;i<devices.length;i++) {
            if(devices[i].type.equals("dimmable_light") || devices[i].type.equals("binary_light"))
            {
                out[count] = devices[i];
                count++;
            }
        }
        return out;
    }
    String[] names(Device[] devices)
    {
        String[] out = new String[devices.length];
        for (int i=0;i<devices.length;i++)
        {
            out[i] = devices[i].name;
        }
        return out;
    }

    private class Section{
        int id,sortOrder;
        String name;
        int[] room_ids;
    }
    private class Room{
        int id,sortOrder,sectionID;
        String name,icon,defaultThermostat;
        Sensor[] defaultSensors;
        int[] device_ids;

    }
    private class Sensor{
        float value;
        String name;
    }
    private class Device{
        int id,roomID;
        String name,type,value;
        View row;
        void setvalue(String val)
        {
            value = val;
        }
        View getview()
        {
            int room_id = getroomid(roomID,rooms);
            if(room_id!=-1) {
                TextView textView = (TextView) row.findViewById(R.id.label);
                TextView textView1 = (TextView) row.findViewById(R.id.section_name);
                TextView textView2 = (TextView) row.findViewById(R.id.roomname);

                textView2.setText(rooms[room_id].name);
                int section_id = getsectionid(rooms[room_id].sectionID, sections);
                if(section_id!=-1)
                    textView1.setText(sections[section_id].name);

                ImageView imageView = (ImageView) row.findViewById(R.id.icon);
                Switch swi = (Switch) row.findViewById(R.id.switch1);
                swi.setOnClickListener(switch_listener);
                SeekBar sb = (SeekBar) row.findViewById(R.id.seekBar);
                sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        setValue(seekBar.getProgress());

                    }
                });
                if(changesst==0)
                    textView.setText(name + " " + type);
                if(!type.equals("dimmable_light") && !type.equals("binary_light"))
                    swi.setVisibility(View.GONE);
                if(Float.parseFloat(value)==0)
                    swi.setChecked(false);
                else
                    swi.setChecked(true);
                if(type.equals("dimmable_light"))
                    sb.setProgress(Integer.parseInt(value));
                else
                    sb.setVisibility(View.GONE);
                return row;
            }
            return null;
        }

        OnClickListener switch_listener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((Switch)v).isChecked())
                    turnOn();
                else
                    turnOff();
            }
        };
        void update(){
             new DeviceGet().execute();

        }
        void turnOn(){
            if(type.equals("dimmable_light") || type.equals("binary_light"))
                new binary_light().execute("turnOn");
        }
        void turnOff(){
            if(type.equals("dimmable_light") || type.equals("binary_light"))
                new binary_light().execute("turnOff");
        }
        void setValue(int value){
            if(type.equals("dimmable_light"))
                new dimable_light().execute(Integer.toString(value));
        }
        private class DeviceGet extends AsyncTask<Void, Void, String>
        {
            String surl = "http://217.168.133.115:8080/api/devices?id="+id;
            String val;
            String txt;
            @Override
            protected String doInBackground(Void... params) {

                txt = null;
                txt = sconnect(surl);
                return null;
            }
            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject c = new JSONObject(txt);
                    id = Integer.parseInt(c.getString("id"));
                    name = c.getString("name");
                    type = c.getString("type");
                    JSONObject properties = new JSONObject(c.getString("properties"));
                    val = properties.getString("value");
                } catch (Exception e)
                {
                }
                setvalue(val);
                row =  getview();
            }
        }
        private class binary_light extends AsyncTask<String, String, String>
        {
            @Override
            protected String doInBackground(String... params) {
                String surl = "http://217.168.133.115:8080/api/callAction?deviceID="+id+"&name="+params[0];
                text = null;
                connect(surl);
                return null;
            }
            @Override
            protected void onPostExecute(String result) {
                update();
            }
        }
        private class dimable_light extends AsyncTask<String, String, String>
        {
            @Override
            protected String doInBackground(String... params) {
                String surl = "http://217.168.133.115:8080/api/callAction?deviceID="+id+"&name=setValue&arg1="+params[0];
                text = null;
                connect(surl);
                return null;
            }
            @Override
            protected void onPostExecute(String result) {
                update();
            }
        }
    }

    private class DeviceAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final Device[] values;

        public DeviceAdapter(Context context,int devicecell, String[] values) {
            super(context, devicecell, values);
            this.context = context;
            this.values = devices2;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.devicecell, parent, false);
            Device dev = values[pos];
            dev.row = rowView;
            return dev.getview();
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return values.length;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return arg0;
        }
    }

}
